package com.avaliacao.scu.service.usuario;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.avaliacao.scu.domain.usuario.Usuario;
import com.avaliacao.scu.domain.usuario.UsuarioRepository;
import com.avaliacao.scu.dto.usuario.UsuarioDTO;

import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class UsuarioService {

	@Autowired
	UsuarioRepository repository;

	public void salvarUsuario(Usuario usuario) {
		if(repository.findByLogin(usuario.getLogin()).isEmpty()){
			repository.save(usuario);
		} else {
			throw new RuntimeException("Login já cadastrado!");
		}
	}

	public List<UsuarioDTO> findAll() {		
		return repository.findAllUsuarios();
	}
	
	
	
	
}
